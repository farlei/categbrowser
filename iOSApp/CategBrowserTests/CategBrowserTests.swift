//
//  CategBrowserTests.swift
//  CategBrowserTests
//
//  Created by Farlei Jose Heinen on 19/03/18.
//  Copyright © 2018 Farlei Heinen. All rights reserved.
//

import XCTest
@testable import CategBrowser

class CategBrowserTests: XCTestCase {
    
    func stubbedResponse(_ filename: String) -> Data {
        @objc class TestClass: NSObject { }
        
        let bundle = Bundle(for: TestClass.self)
        let path = bundle.path(forResource: filename, ofType: "json")
        return (try? Data(contentsOf: URL(fileURLWithPath: path!)))!
    }
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testModelParser() {
        
        let listData = self.stubbedResponse("list")
        do {
            let decoder = JSONDecoder()
            let list = try decoder.decode(List.self, from: listData)
            
            XCTAssertEqual(list.title, "2 x Matched 22\" HP Computer Monitors")
            XCTAssertEqual(list.id, 6533468)
            XCTAssertEqual(list.subtitle, "SKYLARCS TECHNOLOGY LIQUIDATION AUCTION")
            XCTAssertEqual(list.pictureURL, nil)
            
        } catch let error {
            XCTFail(error.localizedDescription)
        }
        
        let categData = self.stubbedResponse("categories")
        do {
            let decoder = JSONDecoder()
            let category = try decoder.decode(Category.self, from: categData)
            
            XCTAssertEqual(category.subCategories?.count, 27)
            XCTAssertEqual(category.name, "Root")
            XCTAssertEqual(category.isLeaf, false)
            
        } catch let error {
            XCTFail(error.localizedDescription)
        }
        
        let pageData = self.stubbedResponse("page")
        do {
            let decoder = JSONDecoder()
            let page = try decoder.decode(Page.self, from: pageData)
            
            XCTAssertEqual(page.list?.count, 20)
            if let first = page.list?.first {
                XCTAssertEqual(first.id, 6527355)
                XCTAssertEqual(first.title, "Retro Baby Blue Kettle")
            } else {
                XCTFail("null first listing")
            }
            
        } catch let error {
            XCTFail(error.localizedDescription)
        }
        
    }
    
    
}
