//
//  LoadingView.swift
//
//  Copyright © 2018 Farlei. All rights reserved.
//

import UIKit
import SnapKit

class LoadingView: UIView {
    
    lazy var activityView: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        return activityView
    }()
    
    lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 22)
        label.textAlignment = .center
        return label
    }()
    
    init(message: String) {
        super.init(frame: CGRect.zero)
        
        self.backgroundColor = Theme.Color.Black.withAlphaComponent(0.8)
        
        self.addSubview(self.infoLabel)
        self.addSubview(self.activityView)
        
        self.infoLabel.text = message
        
        self.activityView.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(100)
            make.height.equalTo(100)
            make.center.equalTo(self)
        }
        
        self.infoLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.activityView.snp.bottom)
            make.left.equalTo(self).offset(Theme.Pad)
            make.right.equalTo(self).offset(-Theme.Pad)
            make.height.equalTo(30)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func start() {
        self.activityView.startAnimating()
    }
    
    func stop() {
        self.activityView.stopAnimating()
    }
}
