//
//  CategoryCell
//
//  Copyright © 2018 Farlei. All rights reserved.
//

import UIKit
import QuartzCore
import SnapKit

class CategoryCell: UITableViewCell {

    static let reuseIdentifier = "CategoryCellReuseIdentifier"
    
    lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textAlignment = .right
        label.textColor = Theme.Color.White
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        
        self.contentView.addSubview(self.infoLabel)
        
        self.infoLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.contentView).offset(Theme.Pad)
            make.right.equalTo(self.contentView.layoutMarginsGuide).offset(-Theme.Pad)
            make.left.equalTo(self.contentView).offset(Theme.Pad)
            make.bottom.lessThanOrEqualTo(self.contentView).offset(-Theme.Pad)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.infoLabel.text = ""
    }
    
    func configWith(category: Category) {
        self.infoLabel.text = category.name
        
        if let leaf = category.isLeaf, leaf == true {
            self.accessoryType = .none
        } else {
            self.accessoryType = .disclosureIndicator
        }
    }
    
}
