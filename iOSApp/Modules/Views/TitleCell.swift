//
//  TitleCell
//
//  Copyright © 2018 Farlei. All rights reserved.
//

import UIKit
import QuartzCore
import SnapKit

class TitleCell: UITableViewCell {

    static let reuseIdentifier = "TitleCellReuseIdentifier"
    
    lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 22)
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.backgroundColor = .white
        
        self.contentView.addSubview(self.infoLabel)
        
        self.infoLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.contentView).offset(Theme.Pad)
            make.right.equalTo(self.contentView).offset(-Theme.Pad)
            make.left.equalTo(self.contentView).offset(Theme.Pad)
            make.bottom.lessThanOrEqualTo(self.contentView).offset(-Theme.Pad)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.infoLabel.text = ""
    }
    
    func configWith(title: String) {
        self.infoLabel.text = title
    }
    
}
