//
//  SearchResultCell.swift
//
//  Copyright © 2018 Farlei. All rights reserved.
//

import UIKit
import AlamofireImage

class SearchResultCell: UITableViewCell {

    static let reuseIdentifier = "SearchResultCellReuseIdentifier"
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    lazy var idLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .gray
        return label
    }()
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .right
        return label
    }()
    
    lazy var thumbnailView: UIImageView = {
        let thumbnailView = UIImageView()
        thumbnailView.contentMode = .scaleAspectFit
        return thumbnailView
    }()
    
    lazy var lineView: UIView = {
        let lineView = UIView()
        lineView.backgroundColor = .darkGray
        return lineView
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        self.contentView.addSubview(self.thumbnailView)
        self.contentView.addSubview(self.nameLabel)
        self.contentView.addSubview(self.idLabel)
        self.contentView.addSubview(self.priceLabel)
        self.contentView.addSubview(self.lineView)
        
        self.thumbnailView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.contentView).offset(10)
            make.left.equalTo(self.contentView).offset(10)
            make.height.equalTo(90)
            make.width.equalTo(90)
            make.bottom.lessThanOrEqualTo(self.contentView).offset(-10)
        }
        
        self.nameLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.contentView).offset(Theme.Pad)
            make.left.equalTo(self.thumbnailView.snp.right).offset(Theme.Pad)
            make.right.equalTo(self.contentView).offset(-Theme.Pad)
        }
        
        self.idLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.nameLabel.snp.bottom).offset(5)
            make.left.equalTo(self.thumbnailView.snp.right).offset(Theme.Pad)
            make.right.equalTo(self.contentView).offset(-Theme.Pad)
        }
        
        self.priceLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.idLabel.snp.bottom).offset(5)
            make.left.equalTo(self.thumbnailView.snp.right).offset(Theme.Pad)
            make.right.equalTo(self.contentView).offset(-Theme.Pad)
            make.bottom.lessThanOrEqualTo(self.contentView).offset(-Theme.Pad)
        }
        
        self.lineView.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(1.0 / UIScreen.main.scale)
            make.left.equalTo(self.contentView).offset(10)
            make.right.equalTo(self.contentView).offset(-10)
            make.bottom.equalTo(self.contentView)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configWith(list: List) {
        
        self.nameLabel.text = list.title
        self.idLabel.text = "\(list.id)"
        self.priceLabel.text = list.displayPrice
        
        if let imageURL = list.pictureURL {
            self.thumbnailView.af_setImage(withURL: imageURL, placeholderImage: UIImage(named:"placeholder"), imageTransition: .crossDissolve(Theme.Animation.fadeTime))
        } else {
            self.thumbnailView.image = UIImage(named:"placeholder")
        }
    }
    
}
