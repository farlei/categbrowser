//
//  Services.swift
//
//  Copyright © 2018 Farlei. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift

public struct Services {
    
    let window: UIWindow
    
    let authManager = AuthManager()
    let api = TrademeAPI()
    
    init(window: UIWindow) {
        self.window = window
    }
}

public class AuthManager {
    
    static let consumerKey = "EC492DE551D8B80639E46E4B81294A50"
    static let consumerSecret = "F08233CEB71DF7265817C0B5DAB90384"
    
    let authKey = "token"
    var accessToken: String?
    
    public static func applicationAuthorizationString() -> String {
        let nonce = NSUUID().uuidString.replacingOccurrences(of: "-", with: "")
        let timestamp = Int(NSDate().timeIntervalSince1970)
        let authString = "OAuth oauth_consumer_key=\"\(AuthManager.consumerKey)\", oauth_nonce=\"\(nonce)\", oauth_signature=\"\(AuthManager.consumerSecret)%26\", oauth_signature_method=\"PLAINTEXT\", oauth_timestamp=\"\(timestamp)\", oauth_version=\"1.0\""
        return authString
    }
    
    init() {
        
    }
}
