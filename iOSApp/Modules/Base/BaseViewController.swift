//
//  BaseViewController.swift
//  

import Foundation
import UIKit
import ReactiveCocoa

/// Defines a public protocol for all the view controllers to follow
public protocol ViewControllerWithViewModel {
    
    associatedtype T: ViewModel
    
    var viewModel: T { get }
    
    init(viewModel: T)
    
    func setupBindings()
    
}

/// A public protocol for views that will be contained in a BaseViewController
public protocol RootView {
    func setupAutoLayoutContraints(parentViewController: UIViewController?)
}

open class NavigationController: UINavigationController {
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
}

open class BaseViewController<M: BaseViewModel, V: RootView>: UIViewController, ViewControllerWithViewModel where V: UIView {
    
    // MARK: Properties
    /// The viewModel that the controller will bind to
    open let viewModel: M
    
    /// A convenience accessor to the ViewControllers type. This avoids having to cast `self.view as? V` when there is a custom view.
    open var rootView: V! { return self.view as! V }
    
    // MARK: Initialisers
    
    required public init(viewModel: M) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.title = self.viewModel.title.value
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: View
    
    override open func loadView() {
        self.view = V()
        self.rootView.setupAutoLayoutContraints(parentViewController: self)
        self.setupBindings()
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        let backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backBarButtonItem
    }
    
    // MARK: - Functions
    
    open func presentModal(_ viewController: UIViewController) {
        let navigationController = NavigationController(rootViewController: viewController)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    open func push(_ viewController: UIViewController) {
        guard let navigationController = self.navigationController else {return}
        navigationController.pushViewController(viewController, animated: true)
    }
    
    /// Create and setup all the ReactiveCocoa bindings to the ViewModel. Subclasses should call `super.setupBindings()`
    open func setupBindings() {
        
        // Create the binding for the title
        self.viewModel.title.producer.skipNil().startWithValues { [weak self] (title) -> () in
            self?.title = title
        }
        
    }
    
    /// Sets a `UILabel` as the `navigationItem` view if it's nil, then sets the attributed text and calls `sizeToFit()`
    open func setAttributedTitle(_ text: NSAttributedString) {
        
        if self.navigationItem.titleView == nil {
            self.navigationItem.titleView = UILabel()
        }
        
        if let label = self.navigationItem.titleView as? UILabel {
            label.attributedText = text
            label.sizeToFit()
        }
    }
    
    
    
}
