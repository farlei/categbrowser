//
//  HomeViewModel.swift
//  iOSApp
//
//  Created by Farlei Jose Heinen on 19/03/2018.
//  Copyright © 2018 Heinen. All rights reserved.
//

import Foundation
import UIKit
import ReactiveSwift

class HomeViewModel: BaseViewModel {
    
    enum RowType {
        case searchResult(list: List)
        case title(text: String)
        case error(text: String)
    }
    
    // MARK: - Properties

    let rootCategoryNumber = "0" // id for the root
    let category = MutableProperty<Category?>(nil) // full category tree
    let lists = MutableProperty<[List]?>([]) // active category listings
    let showCategoryBrowser = MutableProperty<Bool>(false)
    let activeCategory = MutableProperty<Category?>(nil) // active category (current selected one inside the tree)
    
    let rows = MutableProperty<[RowType]>([.title(text: "HomeViewModel.loading".localized)]) // table rows (abstract tableView representation)
    let loading = MutableProperty<Bool>(true)
    
    let iPadMode: Bool // track device type to change side panel style
    
    // MARK: - Initializers

    required init(services: Services, iPadMode: Bool) {
        self.iPadMode = iPadMode
        super.init(services: services)
        
        self.title.value = "HomeViewController.title".localized
        
        // update screen title based on the current category
        self.activeCategory.signal.observeValues { (category) in
            if let category = category {
                self.title.value = category.name.isEmpty ? "HomeViewController.title".localized : category.name
            } else {
                self.title.value = "HomeViewController.title".localized
            }
        }
        
        // remap listing to row type
        self.lists.signal.observeValues { (_) in
            self.remapListRows()
        }
        
        // show loading message on the table view
        self.loading.signal.observeValues { (loading) in
            if loading {
                self.rows.value = [.title(text: "HomeViewModel.loading".localized)]
            }
        }
        
        // when the full category tree is fetched start to load root listings
        self.category.signal.observeValues { (category) in
            if let category = category {
                self.fetchCategoryLists(category: category)
            }
        }
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    required public init(services: Services) {
        fatalError("init(services:) has not been implemented")
    }
    
    // MARK: - Methods
    
    func switchBrowserPanel() {
        self.showCategoryBrowser.value = !self.showCategoryBrowser.value
    }
    
    func closeBrowserPanel() {
        self.showCategoryBrowser.value = false
    }
    
    /// generate table rows based on current status
    func remapListRows() {
        if let lists = self.lists.value {
            if lists.count > 0 {
                self.rows.value = lists.map({ (list) -> RowType in
                    .searchResult(list: list)
                })
            } else {
                self.rows.value = [.title(text: "HomeViewModel.empty".localized)]
            }
        } else {
            self.rows.value = [.error(text: "HomeViewModel.error.lists".localized)]
        }
    }
    
    func fetchAllCategories() {
        self.loading.value = true
        self.services.api.allCategories { (category) in
            self.loading.value = false
            self.category.value = category
        }
    }
    
    func fetchCategoryLists(category: Category) {
        
        // dont load same category more then once
        if let active = self.activeCategory.value {
            if active.number == category.number { return }
        }
        
        self.loading.value = true
        let number = category.number.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty ? self.rootCategoryNumber : category.number
        self.services.api.categorySearch(categoryNumber: number) { (lists) in
            self.loading.value = false
            self.activeCategory.value = category
            self.lists.value = lists
        }
    }

}
