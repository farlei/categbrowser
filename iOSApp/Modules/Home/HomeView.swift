//
//  HomeView.swift
//  iOSApp
//
//  Created by Farlei Jose Heinen on 19/03/2018.
//  Copyright © 2018 Heinen. All rights reserved.
//

import Foundation
import UIKit

class HomeView: UIView, RootView {
    
    let iPadPanelRatio = 0.3 // panel use 30% of the screen on iPad
    let iPhonePanelDelta = -110 // panel dont cover the thumbnails on iPhone
    
    // MARK: - Properties
    
    var iPadMode = false

    lazy var tableView: UITableView = {
        let tableView = UITableView()
        
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 150
        tableView.allowsSelection = true
        tableView.separatorStyle = .none
        
        tableView.register(SearchResultCell.self, forCellReuseIdentifier: SearchResultCell.reuseIdentifier)
        tableView.register(TitleCell.self, forCellReuseIdentifier: TitleCell.reuseIdentifier)
        
        return tableView
    }()
    
    lazy var sideContainerView: UIView = {
        let sideView = UIView()
        sideView.backgroundColor = UIColor.lightGray
        sideView.clipsToBounds = true
        return sideView
    }()

    // MARK: - Initializers

    init() {
        super.init(frame: .zero)
        self.addSubview(self.tableView)
        self.addSubview(self.sideContainerView)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Layout
    
    func setupAutoLayoutContraints(parentViewController: UIViewController?) {
        
        self.tableView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self)
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.bottom.equalTo(self.safeAreaLayoutGuide)
        }
        
        self.sideContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(10)
            make.left.equalTo(self.snp.right)
            make.width.equalTo(self).offset(iPhonePanelDelta)
            make.bottom.equalTo(self.safeAreaLayoutGuide).offset(-10)
        }
    }

    override open func layoutSubviews() {
        super.layoutSubviews()
        
        // generate rounding corners only on the left side of the view
        let path = UIBezierPath(roundedRect:self.sideContainerView.bounds,
                                byRoundingCorners:[.topLeft, .bottomLeft],
                                cornerRadii: CGSize(width: 8, height:  8))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.sideContainerView.layer.mask = maskLayer
    }

    // MARK: - Methods
    
    /// change view constraints and animations if the device is an iPad
    open func enableiPadMode() {
        
        self.iPadMode = true
        
        self.tableView.snp.removeConstraints()
        self.tableView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self)
            make.left.equalTo(self)
            make.right.equalTo(self.sideContainerView.snp.left).offset(-5)
            make.bottom.equalTo(self.safeAreaLayoutGuide)
        }
        
        self.sideContainerView.snp.removeConstraints()
        self.sideContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(10)
            make.left.equalTo(self.snp.right)
            make.width.equalTo(self).multipliedBy(iPadPanelRatio)
            make.bottom.equalTo(self.safeAreaLayoutGuide).offset(-10)
        }
    }
    
    open func showPanel() {
        
        self.layoutIfNeeded()
        self.sideContainerView.snp.removeConstraints()
        self.sideContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(10)
            make.right.equalTo(self)
            if self.iPadMode { make.width.equalTo(self).multipliedBy(iPadPanelRatio) }
            else { make.width.equalTo(self).offset(iPhonePanelDelta) }
            make.bottom.equalTo(self.safeAreaLayoutGuide).offset(-10)
        }
        
        UIView.animate(withDuration: Theme.Animation.slideTime) {
            self.layoutIfNeeded()
        }
    }
    
    open func hidePanel() {
        
        self.layoutIfNeeded()
        self.sideContainerView.snp.removeConstraints()
        self.sideContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(10)
            make.left.equalTo(self.snp.right)
            if self.iPadMode { make.width.equalTo(self).multipliedBy(iPadPanelRatio) }
            else { make.width.equalTo(self).offset(iPhonePanelDelta) }
            make.bottom.equalTo(self.safeAreaLayoutGuide).offset(-10)
        }
        
        UIView.animate(withDuration: Theme.Animation.slideTime) {
            self.layoutIfNeeded()
        }
    }

}
