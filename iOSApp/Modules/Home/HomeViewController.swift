//
//  HomeViewController.swift
//  iOSApp
//
//  Created by Farlei Jose Heinen on 19/03/2018.
//  Copyright © 2018 Heinen. All rights reserved.
//

import Foundation
import UIKit

protocol HomeViewControllerDelegate: class {
    
}

class HomeViewController: BaseViewController<HomeViewModel, HomeView>, UITableViewDelegate, UITableViewDataSource {

    // MARK: - Properties

    weak var delegate: HomeViewControllerDelegate?

    var navController: UINavigationController?
    
    // MARK: - Initializers

    required init(viewModel: HomeViewModel) {
        super.init(viewModel: viewModel)
        
        if viewModel.iPadMode {
            self.rootView.enableiPadMode()
        }
        
        self.rootView.tableView.delegate = self
        self.rootView.tableView.dataSource = self
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "reload")?.withRenderingMode(.alwaysOriginal), style: UIBarButtonItemStyle.plain, target: self, action: #selector(tapReload))
        self.navigationItem.leftBarButtonItem?.isEnabled = false
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "HomeViewController.browse".localized, style: .plain, target: self, action: #selector(tapBrowse))
        self.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.lightGray], for: .disabled)
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    @available(*, unavailable)
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setupBindings() {
        super.setupBindings()
        
        // only enable the bar actions after the categories are available
        self.viewModel.category.signal.observeValues { (category) in
            if category != nil {
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                self.navigationItem.leftBarButtonItem?.isEnabled = true
            } else {
                self.navigationItem.rightBarButtonItem?.isEnabled = false
                self.navigationItem.leftBarButtonItem?.isEnabled = false
                self.viewModel.closeBrowserPanel()
            }
        }
        
        // setup category list when data fetched
        self.viewModel.category.signal.observeValues { (category) in
            if category != nil {
                self.setupSidePanel()
            }
        }
        
        // update the tableView when new data is available
        self.viewModel.rows.signal.observeValues { (_) in
            self.rootView.tableView.reloadData()
        }
        
        // open/close the side panel
        self.viewModel.showCategoryBrowser.signal.observeValues { (show) in
            if show {
                self.rootView.showPanel()
            } else {
                self.rootView.hidePanel()
            }
        }
    }
    
    // MARK: - View

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // start to fetch the categories
        self.rootView.tableView.reloadData()
        self.viewModel.fetchAllCategories()
    }
    
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.navController?.view.frame = self.rootView.sideContainerView.bounds
    }
    
    // add the side panel category browser as a child view controller
    func setupSidePanel() {
        
        guard let category = self.viewModel.category.value else { return }
        
        let viewModel = CategoryViewModel(services: self.viewModel.services, category: category)
        let viewController = CategoryViewController(viewModel: viewModel)
        viewController.delegate = self
        
        self.navController = UINavigationController(rootViewController: viewController)
        navController?.view.frame = self.rootView.sideContainerView.bounds
        self.rootView.sideContainerView.addSubview(navController!.view)
        self.addChildViewController(navController!)
    }

    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.rows.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = self.viewModel.rows.value[indexPath.row]
        switch item {
        case .searchResult(let list):
            let cell = self.rootView.tableView.dequeueReusableCell(withIdentifier: SearchResultCell.reuseIdentifier) as! SearchResultCell
            cell.configWith(list: list)
            return cell
        case .title(let text):
            let cell = self.rootView.tableView.dequeueReusableCell(withIdentifier: TitleCell.reuseIdentifier) as! TitleCell
            cell.configWith(title: text)
            return cell
        case .error(let text):
            let cell = self.rootView.tableView.dequeueReusableCell(withIdentifier: TitleCell.reuseIdentifier) as! TitleCell
            cell.configWith(title: text)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // if category browser is open and a listing is selected close the panel first (if not iPad)
        if self.viewModel.showCategoryBrowser.value && !self.viewModel.iPadMode {
            self.viewModel.closeBrowserPanel()
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        
        // only open list detail if panel is closed
        let item = self.viewModel.rows.value[indexPath.row]
        switch item {
        case .searchResult(let list):
            let viewModel = DetailViewModel(services: self.viewModel.services, listingId: list.id)
            let viewController = DetailViewController(viewModel: viewModel)
            self.navigationController?.pushViewController(viewController, animated: true)
        default:
            break
        }
    }
    
    // MARK: - Methods

    @objc func tapBrowse() {
        self.viewModel.switchBrowserPanel()
    }
    
    // reset to the category root and reload listings
    @objc func tapReload() {
        
        guard let category = self.viewModel.category.value else { return }
        
        let viewModel = CategoryViewModel(services: self.viewModel.services, category: category)
        let viewController = CategoryViewController(viewModel: viewModel)
        viewController.delegate = self
        
        self.navController?.setViewControllers([viewController], animated: true)
        self.viewModel.fetchCategoryLists(category: category)
    }
    
}

// actions from the current category listed on the side panel
extension HomeViewController: CategoryViewControllerDelegate {
    
    func categoryViewControllerDidRequestClose(_ categoryViewController: CategoryViewController) {
        self.viewModel.closeBrowserPanel()
    }
    
    func categoryViewControllerDidSelectCategory(_ categoryViewController: CategoryViewController, category: Category) {
        self.viewModel.fetchCategoryLists(category: category)
    }
}
