//
//  CategoryViewModel.swift
//  iOSApp
//
//  Created by Farlei Jose Heinen on 20/03/2018.
//  Copyright © 2018 Heinen. All rights reserved.
//

import Foundation
import UIKit

class CategoryViewModel: BaseViewModel {
    
    enum RowType {
        case category(category: Category)
    }
    
    // MARK: - Properties

    let category: Category
    let rows: [RowType]
    
    // MARK: - Initializers

    required init(services: Services, category: Category) {
        self.category = category
        
        // map the first level of subCategories
        if let subCategories = category.subCategories {
            self.rows = subCategories.map({ (category) -> RowType in
                return .category(category: category)
            })
        } else {
            self.rows = []
        }
        
        super.init(services: services)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    required public init(services: Services) {
        fatalError("init(services:) has not been implemented")
    }
    
    // MARK: - Methods

}
