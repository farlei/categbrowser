//
//  CategoryViewController.swift
//  iOSApp
//
//  Created by Farlei Jose Heinen on 20/03/2018.
//  Copyright © 2018 Heinen. All rights reserved.
//

import Foundation
import UIKit

protocol CategoryViewControllerDelegate: class {
    func categoryViewControllerDidSelectCategory(_ categoryViewController: CategoryViewController, category: Category)
    func categoryViewControllerDidRequestClose(_ categoryViewController: CategoryViewController)
}

class CategoryViewController: BaseViewController<CategoryViewModel, CategoryView>, UITableViewDelegate, UITableViewDataSource {

    // MARK: - Properties

    weak var delegate: CategoryViewControllerDelegate?

    // MARK: - Initializers

    required init(viewModel: CategoryViewModel) {
        super.init(viewModel: viewModel)
        
        self.rootView.tableView.delegate = self
        self.rootView.tableView.dataSource = self
    }
    
    @available(*, unavailable)
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setupBindings() {
        super.setupBindings()
        
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "CategoryViewController.categories".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: Theme.Color.Black]
        let doneButton = UIBarButtonItem(title: "CategoryViewController.close".localized, style: .plain, target: self, action: #selector(tapDone))
        doneButton.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.darkGray,
                                           NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)], for: .normal)
        self.navigationItem.rightBarButtonItem = doneButton
        
        self.rootView.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.delegate?.categoryViewControllerDidSelectCategory(self, category: self.viewModel.category)
    }

    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = self.viewModel.rows[indexPath.row]
        switch item {
        case .category(let category):
            let cell = self.rootView.tableView.dequeueReusableCell(withIdentifier: CategoryCell.reuseIdentifier) as! CategoryCell
            cell.configWith(category: category)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = self.viewModel.rows[indexPath.row]
        switch item {
        case .category(let category):
            
            if let leaf = category.isLeaf, leaf == false {
                let viewModel = CategoryViewModel(services: self.viewModel.services, category: category)
                let viewController = CategoryViewController(viewModel: viewModel)
                viewController.delegate = self.delegate
                self.navigationController?.pushViewController(viewController, animated: true)
            } else {
                self.delegate?.categoryViewControllerDidSelectCategory(self, category: category)
            }
        }
    }

    // MARK: - Methods
    
    @objc func tapDone() {
        self.delegate?.categoryViewControllerDidRequestClose(self)
    }
}
