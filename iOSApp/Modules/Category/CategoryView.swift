//
//  CategoryView.swift
//  iOSApp
//
//  Created by Farlei Jose Heinen on 20/03/2018.
//  Copyright © 2018 Heinen. All rights reserved.
//

import Foundation
import UIKit

class CategoryView: UIView, RootView {
    
    // MARK: - Properties

    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = UIColor.lightGray
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 150
        tableView.allowsSelection = true
        tableView.separatorStyle = .none
        
        tableView.register(CategoryCell.self, forCellReuseIdentifier: CategoryCell.reuseIdentifier)
        
        return tableView
    }()

    // MARK: - Initializers

    init() {
        super.init(frame: .zero)
        self.addSubview(self.tableView)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Layout
    
    func setupAutoLayoutContraints(parentViewController: UIViewController?) {
        
        self.tableView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self)
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.bottom.equalTo(self.safeAreaLayoutGuide)
        }
    }

    // MARK: - Methods

}
