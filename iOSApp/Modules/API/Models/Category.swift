//
//  Category
//
//  Created by Farlei Jose Heinen
//  Copyright © 2018 Farlei. All rights reserved.
//

import Foundation

public enum AreaOfBusiness: Int, Codable {
    case All = 0
    case Marketplace = 1
    case Property = 2
    case Motors = 3
    case Jobs = 4
    case Services = 5
}

public struct Category: Codable {
    
    let number: String
    let name: String
    let path: String?
    let areaOfBusiness: AreaOfBusiness?
    let subCategories: [Category]?
    let isLeaf: Bool?
    
    enum CodingKeys: String, CodingKey {
        case number = "Number"
        case name = "Name"
        case path = "Path"
        case subCategories = "Subcategories"
        case isLeaf = "IsLeaf"
        case areaOfBusiness = "AreaOfBusiness"
    }
    
}
