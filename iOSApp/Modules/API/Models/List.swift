//
//  List
//
//  Created by Farlei Jose Heinen
//  Copyright © 2018 Farlei. All rights reserved.
//

import Foundation

public struct Page: Codable {
    
    let totalCount: Int
    let page: Int
    let pageSize: Int
    let list: [List]?
    
    enum CodingKeys: String, CodingKey {
        case totalCount = "TotalCount"
        case page = "Page"
        case pageSize = "PageSize"
        case list = "List"
    }
}

public struct PhotoIndex: Codable {
    let key: Int
    let value: Photo
    
    enum CodingKeys: String, CodingKey {
        case key = "Key"
        case value = "Value"
    }
}

public struct Photo: Codable {
    let thumbnail: URL?
    let large: URL?
    
    enum CodingKeys: String, CodingKey {
        case thumbnail = "Thumbnail"
        case large = "Large"
    }
    
    public func imageURL() -> URL? {
        if self.large != nil { return self.large }
        return self.thumbnail
    }
}

public struct List: Codable {
    
    let id: Int
    let title: String
    let subtitle: String?
    let displayPrice: String
    let pictureURL: URL?
    let photos: [PhotoIndex]?
    
    enum CodingKeys: String, CodingKey {
        case id = "ListingId"
        case title = "Title"
        case displayPrice = "PriceDisplay"
        case pictureURL = "PictureHref"
        case subtitle = "Subtitle"
        case photos = "Photos"
    }
    
}
