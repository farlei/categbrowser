//
//  TrademeAPI
//
//  Copyright © 2017 Farlei. All rights reserved.
//

import Foundation
import Alamofire

// https://developer.trademe.co.nz/api-reference/

public class TrademeAPI {
    
    static let maxRows = 20 // max rows per page
    static let apiVersion = "v1"
    
    /// return a category root object with the full category tree
    public func allCategories(done: @escaping (_ categories: Category?) -> Void) {
        
        let endpoint = TrademeEndpoint.allCategories
        Alamofire.request(endpoint).responseData { (response) in
            if let jsonData = response.data {
                do {
                    let decoder = JSONDecoder()
                    let category = try decoder.decode(Category.self, from: jsonData)
                    done(category)
                    return
                } catch let error {
                    print(error)
                }
            }
            done(nil)
        }
    }
    
    /// return the top Lists from a category
    public func categorySearch(categoryNumber: String, done: @escaping (_ lists: [List]?) -> Void) {
        
        let endpoint = TrademeEndpoint.categorySearch(categoryNumber: categoryNumber)
        Alamofire.request(endpoint).responseData { (response) in
            if let jsonData = response.data {
                do {
                    let decoder = JSONDecoder()
                    let page = try decoder.decode(Page.self, from: jsonData)
                    done(page.list)
                    return
                } catch let error {
                    print(error)
                }
            }
            done(nil)
        }
    }
    
    /// return the details for one listing
    public func listingDetail(listingId: Int, done: @escaping (_ list: List?) -> Void) {
        
        let endpoint = TrademeEndpoint.listingDetail(listingId: listingId)
        Alamofire.request(endpoint).responseData { (response) in
            if let jsonData = response.data {
                do {
                    let decoder = JSONDecoder()
                    let list = try decoder.decode(List.self, from: jsonData)
                    done(list)
                    return
                } catch let error {
                    print(error)
                }
            }
            done(nil)
        }
    }
}

/// endpoint definition
public enum TrademeEndpoint: URLConvertible, URLRequestConvertible {
    
    case allCategories
    case categorySearch(categoryNumber: String)
    case listingDetail(listingId: Int)
    
    public var baseURL: URL {
        switch self {
            default: return URL(string: "https://api.tmsandbox.co.nz")!
        }
    }
    
    public var path: String {
        switch self {
            case .allCategories: return "/\(TrademeAPI.apiVersion)/Categories.json"
            case .categorySearch: return "/\(TrademeAPI.apiVersion)/Search/General.json"
            case .listingDetail(let listingId): return "/\(TrademeAPI.apiVersion)/Listings/\(listingId).json"
        }
    }
    
    public var parameters: [String: Any]? {
        switch self {
        case .categorySearch(let categoryNumber):
            return ["rows": "\(TrademeAPI.maxRows)", "photo_size": "Thumbnail", "category": categoryNumber]
        default:
            return [:]
        }
    }
    
    public var httpMethod: Alamofire.HTTPMethod {
        switch self {
        default:
            return .get
        }
    }
    
    public var needAuth: Bool {
        switch self {
        default:
            return true
        }
    }
    
    public var parameterEncoding: Alamofire.ParameterEncoding {
        switch self {
            default:
                return URLEncoding.queryString
        }
    }
    
    public func asURL() throws -> URL {
        return self.baseURL.appendingPathComponent(self.path)
    }
    
    public func asURLRequest() throws -> URLRequest {
        
        var request = URLRequest(url: try self.asURL())
        request.httpMethod = self.httpMethod.rawValue
        request = try self.parameterEncoding.encode(request, with: self.parameters)
        
        if self.needAuth {
            request.setValue(AuthManager.applicationAuthorizationString(), forHTTPHeaderField: "Authorization")
        }
        
        return request
    }
}
