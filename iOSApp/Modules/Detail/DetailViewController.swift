//
//  DetailViewController.swift
//  iOSApp
//
//  Created by Farlei Jose Heinen on 21/03/2018.
//  Copyright © 2018 Heinen. All rights reserved.
//

import Foundation
import UIKit

protocol DetailViewControllerDelegate: class {
    
}

class DetailViewController: BaseViewController<DetailViewModel, DetailView> {

    // MARK: - Properties

    weak var delegate: DetailViewControllerDelegate?

    // MARK: - Initializers

    required init(viewModel: DetailViewModel) {
        super.init(viewModel: viewModel)
    }
    
    @available(*, unavailable)
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setupBindings() {
        super.setupBindings()
        
        self.viewModel.loading.signal.observeValues { (loading) in
            self.rootView.startLoading(loading)
        }
        
        self.viewModel.listing.signal.observeValues { (listing) in
            if let listing = listing {
                
                if let imageURL = listing.photos?.first?.value.imageURL() {
                    self.rootView.thumbnailView.af_setImage(withURL: imageURL, placeholderImage: UIImage(named:"placeholder"), imageTransition: .crossDissolve(Theme.Animation.fadeTime))
                } else {
                    self.rootView.thumbnailView.image = UIImage(named:"placeholder")
                }
                
                self.rootView.nameLabel.text = listing.title
                self.rootView.idLabel.text = "\(listing.id)"
                self.rootView.subtitleLabel.text = listing.subtitle ?? ""
                
            } else {
                self.rootView.thumbnailView.image = UIImage(named:"placeholder")
                self.rootView.nameLabel.text = "DetailViewController.Error".localized
                self.rootView.idLabel.text = ""
                self.rootView.subtitleLabel.text = ""
            }
        }
        
        self.viewModel.fetchData()
    }

    // MARK: - View


    // MARK: - Methods

}
