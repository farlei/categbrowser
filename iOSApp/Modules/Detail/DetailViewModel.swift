//
//  DetailViewModel.swift
//  iOSApp
//
//  Created by Farlei Jose Heinen on 21/03/2018.
//  Copyright © 2018 Heinen. All rights reserved.
//

import Foundation
import UIKit
import ReactiveSwift

class DetailViewModel: BaseViewModel {
    
    // MARK: - Properties

    let listingId: Int
    let listing = MutableProperty<List?>(nil)
    let loading = MutableProperty<Bool>(false)
    
    // MARK: - Initializers

    required init(services: Services, listingId: Int) {
        self.listingId = listingId
        super.init(services: services)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    required public init(services: Services) {
        fatalError("init(services:) has not been implemented")
    }
    
    // MARK: - Methods
    
    func fetchData() {
        self.loading.value = true
        self.services.api.listingDetail(listingId: self.listingId) { (listing) in
            self.listing.value = listing
            self.loading.value = false
        }
    }

}
