//
//  DetailView.swift
//  iOSApp
//
//  Created by Farlei Jose Heinen on 21/03/2018.
//  Copyright © 2018 Heinen. All rights reserved.
//

import Foundation
import UIKit

class DetailView: UIView, RootView {
    
    // MARK: - Properties
    
    lazy var thumbnailView: UIImageView = {
        let thumbnailView = UIImageView()
        thumbnailView.contentMode = .scaleAspectFit
        return thumbnailView
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    
    lazy var idLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .gray
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()

    lazy var loadingView: LoadingView = {
        return LoadingView(message: "DetailView.loading".localized)
    }()

    // MARK: - Initializers

    init() {
        super.init(frame: .zero)
        
        self.backgroundColor = Theme.Color.White
        
        self.addSubview(self.thumbnailView)
        self.addSubview(self.nameLabel)
        self.addSubview(self.idLabel)
        self.addSubview(self.subtitleLabel)
        self.addSubview(self.loadingView)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Layout
    
    func setupAutoLayoutContraints(parentViewController: UIViewController?) {
        
        self.thumbnailView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self).offset(Theme.Pad)
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.height.equalTo(self).multipliedBy(0.25)
        }
        
        self.nameLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.thumbnailView.snp.bottom).offset(Theme.Pad * 2)
            make.left.equalTo(self).offset(Theme.Pad)
            make.right.equalTo(self).offset(-Theme.Pad)
        }
        
        self.idLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.nameLabel.snp.bottom).offset(Theme.Pad)
            make.left.equalTo(self).offset(Theme.Pad)
            make.right.equalTo(self).offset(-Theme.Pad)
        }
        
        self.subtitleLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.idLabel.snp.bottom).offset(Theme.Pad * 2)
            make.left.equalTo(self).offset(Theme.Pad)
            make.right.equalTo(self).offset(-Theme.Pad)
        }
        
        
        self.loadingView.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(self)
        }

    }

    // MARK: - Methods
    
    func startLoading(_ loading: Bool) {
        if loading {
            self.loadingView.start()
            self.loadingView.isHidden = false
        } else {
            self.loadingView.stop()
            self.loadingView.isHidden = true
        }
    }

}
