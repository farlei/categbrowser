//
//  AppDelegate.swift
//  CategBrowser
//
//  Created by Farlei Jose Heinen on 19/03/18.
//  Copyright © 2018 Farlei Heinen. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = UIColor.white
        
        // run initial apearance setup
        Theme.setup()
        
        // create default service to be injeted in the app flow
        let services = Services(window: window)
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            // is iPhone
            window.rootViewController = NavigationController(rootViewController: HomeViewController(viewModel: HomeViewModel(services: services, iPadMode: false)))
        } else {
            // is iPad
            window.rootViewController = NavigationController(rootViewController: HomeViewController(viewModel: HomeViewModel(services: services, iPadMode: true)))
        }
        
        window.makeKeyAndVisible()
        self.window = window
        
        return true
    }
    
    // set different orientations for iPhone or iPad
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
           return [.portrait]
        } else {
           return [.landscapeLeft, .landscapeRight]
        }
    }
}
