//
//  Theme.swift
//
//  Created by Farlei Heinen

import UIKit

public extension UIColor {
    public convenience init(hex: Int) {
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        self.init(red: components.R, green: components.G, blue: components.B, alpha: 1)
    }
}

public extension UIImage {
    
    public class func imageForColor(_ color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        
        UIGraphicsBeginImageContext(rect.size)
        
        let context = UIGraphicsGetCurrentContext();
        context?.setFillColor(color.cgColor);
        context?.fill(rect);
        
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return image!;
        
    }
}

//  global app style definition
public struct Theme {
    
    public struct Color {
        public static let Black = UIColor(hex: 0x000000)
        public static let White = UIColor(hex: 0xFFFFFF)
        public static let Blue = UIColor(hex: 0x1870FF)
        public static let Yellow = UIColor(hex: 0xfbc53f)
    }
    
    public struct Animation {
        public static let fadeTime = Double(0.2)
        public static let slideTime = Double(0.3)
    }
    
    public static let Pad = 10
    
    public static func setup() {
        
        UINavigationBar.appearance().setBackgroundImage(UIImage.imageForColor(Theme.Color.Yellow), for: UIBarMetrics.default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().backgroundColor = Theme.Color.Yellow
        UINavigationBar.appearance().barTintColor = Theme.Color.Yellow
        UINavigationBar.appearance().tintColor = Theme.Color.Yellow
        
        UINavigationBar.appearance().backIndicatorImage = UIImage(named: "icon-back")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = UIImage(named: "icon-back")
        
        UIBarButtonItem.appearance().tintColor = Theme.Color.White
        
        UISearchBar.appearance().backgroundColor = Theme.Color.Yellow

        let titleTextAttributes = [NSAttributedStringKey.foregroundColor: Theme.Color.Black]
        UINavigationBar.appearance().titleTextAttributes = titleTextAttributes
        
        UIBarButtonItem.appearance().setTitleTextAttributes(titleTextAttributes, for: .normal)
    }
}
