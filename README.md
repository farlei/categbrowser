# Trademe Category Browser

## Instalation

### Dependencies

 - Xcode 9.2
 - Carthage 0.27 or higher

### Carthage

The project uses Carthage as the package manager. To bootstrap the dependencies just run this script in the iOSApp folder:

```bash
$ ./bootstrap
```
